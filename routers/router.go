package routers

import (
	"AlanGo/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("users",
			beego.NSRouter("/register", &controllers.UserController{}, "POST:Register"),
			// beego.NSRouter("/ForgetTpl", &controllers.UserController{}, "get:ForgetTpl"),
			beego.NSRouter("/login", &controllers.UserController{}, "POST:Login"),
			beego.NSRouter("/findPeople", &controllers.UserController{}, "POST:QueryFriend"),
			beego.NSRouter("/addFriend", &controllers.UserController{}, "POST:AddFriend"),
			// beego.NSRouter("/ForgetAccount", &controllers.UserController{}, "POST:ForgetAccount"),
			beego.NSRouter("/list", &controllers.TodoLists{}, "GET:GetlistTpl"),
			beego.NSRouter("/saveTodo", &controllers.TodoLists{}, "POST:SaveTodo"),
			beego.NSRouter("/delTodo", &controllers.TodoLists{}, "POST:DelTodo"),
			beego.NSRouter("/updateTodo", &controllers.TodoLists{}, "POST:UpdateTodo"),
			beego.NSRouter("/todoList", &controllers.TodoLists{}, "GET:TodoLists"),
		),
		beego.NSNamespace("back",
			beego.NSRouter("/register", &controllers.UserController{}, "POST:Register"),
			// beego.NSRouter("/ForgetTpl", &controllers.UserController{}, "get:ForgetTpl"),
			beego.NSRouter("/login", &controllers.UserController{}, "POST:Login"),
		),
		// beego.NSNamespace("",
		// 	beego.NSRouter("/Register", &controllers.UserController{}, "POST:Register"),
		// 	beego.NSRouter("/ForgetTpl", &controllers.UserController{}, "get:ForgetTpl"),
		// 	beego.NSRouter("/Login", &controllers.UserController{}, "POST:Login"),
		// 	beego.NSRouter("/ForgetAccount", &controllers.UserController{}, "POST:ForgetAccount"),
		// 	beego.NSRouter("/List", &controllers.TodoLists{}, "GET:GetlistTpl"),
		// 	beego.NSRouter("/saveTodo", &controllers.TodoLists{}, "GET:SaveTodo"),
		// 	beego.NSRouter("/updateTodo", &controllers.TodoLists{}, "GET:UpdateTodo"),
		// 	beego.NSRouter("/todoList", &controllers.TodoLists{}, "GET:TodoLists"),
		// ),
		// beego.NSNamespace("users",
		// 	beego.NSRouter("/Register", &controllers.UserController{}, "GET:Register"),
		// ),
	)
	beego.AddNamespace(ns)
}
