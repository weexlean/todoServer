package models

import "container/list"

type EventType int

const (
	EVENT_JOIN = iota
	EVENT_LEAVE
	EVENT_MESSAGE
)

type Event struct {
	Type      EventType
	UserID    string
	SessionID string
	Timestamp string
	Content   string
}

const archiveSize = 20

var archive = list.New()
