package utils

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/astaxie/beego"
)

func InitBootStrap() {
	beego.SetLogger("file", `{"filename":"logs/logs.log"}`)
	graceful, _ := beego.AppConfig.Bool("Graceful")
	if !graceful {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
		go handleSignals(sigs)
	}
}

func handleSignals(c chan os.Signal) {
	switch <-c {
	case syscall.SIGINT, syscall.SIGTERM:
		fmt.Println("Shutdown quickly, bye...")
	case syscall.SIGQUIT:
		fmt.Println("Shutdown gracefully, bye...")
		// do graceful shutdown
	}
	os.Exit(0)
}
