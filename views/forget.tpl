<!DOCTYPE html>
<html>
<head>
	<title>Forget</title>
    <script type="text/javascript" src="../../static/js/jquery.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
			$("#commit").click(function(event) {
				let account = $("#Account");
				let name = $("#Name");
				let password = $("#Password");
				let passwordOnce = $("#PasswordOnce");
				console.log("1");
				if (account.val() == "") {
					return;
				}
				console.log(account.val().count);
				console.log("2");

				$.post('/path/to/file', {param1: 'value1'}, function(data, textStatus, xhr) {
					/*optional stuff to do after success */
				});

				
				$.get('/path/to/file', function(data) {
					/*optional stuff to do after success */
				});
				// let account = $("#account").val();
				// if (account.count <= 0) {
				// 	window.alert("account isn't nill")
				// 	return ;
				// }
				// $.post('/v1/users/ForgetAccount', {"account": account.val()}, function(data, textStatus, xhr) {
				// 	console.log(data);
				// 	if (textStatus == "success") {
				// 		console.log($.type(data));
				// 		let json = eval(data);
				// 		console.log(json["code"]);
				// 	};
				//  });
			});
	});

    </script>
    <style type="text/css">
    	* {
    		margin: 0px;
    		padding: 0px;
    	}

			#content {
				text-align: center;
				position: absolute;
				border-radius: 20px;
				border: 1px solid blue;
				background: #cccccc;
				width: 300px;
				height: 330px;
				top: 50%;
				left: 50%;
				transform: translate(-150px, -150px);
			}

		.top label {
				font-size: 20px;
				font-weight: inherit;
				color: black;
				line-height: 50px;
    		width: 100%;
    		margin: auto;
    	}

    	.line {
    		background: blue;
    		height: 1px;
    		width: 100%;
    	}

    	#forgetDiv .forgetDivItem {
				position: relative;
				margin-left: 10%;
				width: 80%;
				height: 40px;
				margin-top: 10px;
			}

			.forgetDivItem img {
				position: absolute;
				width: 20px;
				height: 20px;
				margin-left: 10px;
				margin-top: 10px;
				background: blue;
			}

			.forgetDivItem .forgetDivItemInput {
				width: 100%;
				height: 40px;
				text-indent: 40px;
			}

			#forgetDivItemReturn {
					margin-top: 30px;
			}

			#forgetDivItemReturn button {
				width: 100%;
				height: 40px;
				border-width: 0px;
				background: white;
				text-align: center;
			}
    </style>
</head>
<body>
	<div id="content">
		<div class="top"><label>Please input your account! </label></div>
		<div class="line"></div>
		<div id="forgetDiv">

			<div class="forgetDivItem">
				<img src="" alt="">
				<input class="forgetDivItemInput" id="Account" placeholder="Account" type="text" name="" value="">
			</div>

			<div class="forgetDivItem">
				<img src="" alt="">
				<input class="forgetDivItemInput" id="Name" placeholder="Name" type="text" name="" value="">
			</div>

			<div class="forgetDivItem">
				<img src="" alt="">
				<input class="forgetDivItemInput" id="Password" placeholder="Password" type="text" name="" value="">
			</div>

			<div class="forgetDivItem">
				<img src="" alt="">
				<input class="forgetDivItemInput" id="PasswordOnce" placeholder="Password Once" type="text" name="" value="">
			</div>

			<div class="forgetDivItem CommitBtn" id="forgetDivItemReturn">
				<button id="commit" type="button" name="button">Commit on!</button>
			</div>
		</div>
	</div>
</body>
</html>
