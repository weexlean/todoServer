<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1">
    <title>mytodos</title>
    <link href=../../static/css/app.0105782f779588db7d250a629b079457.css rel=stylesheet>
  </head>
  <body>
    <div id=app></div>
    <script type=text/javascript src=../../static/js/manifest.3ad1d5771e9b13dbdad2.js></script>
    <script type=text/javascript src=../../static/js/vendor.92a434c973eefec2db1a.js></script>
    <script type=text/javascript src=../../static/js/app.e8977038a47648a360cd.js></script>
  </body>
  </html>
