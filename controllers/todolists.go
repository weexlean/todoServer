package controllers

import (
	"AlanGo/models"
	"strings"
	"time"

	"github.com/astaxie/beego"
)

type TodoLists struct {
	MainController
}

func (c *TodoLists) GetlistTpl() {
	c.TplName = "list.tpl"
}

//DelTodo method.
func (this *TodoLists) DelTodo() {
	user, code := this.CheckToken()
	res := this.InitResponseModel()
	beego.Debug(res)
	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		this.Data["json"] = res
		beego.Debug(res)
		this.ServeJSON()
		return
	}

	delTodos := this.GetString("todos")
	delTodoLists := strings.Split(delTodos, ",")
	if len(delTodoLists) == 0 {
		res.Msg = ErrParmars
		res.Code = ErrParmarsCode
		this.Data["json"] = res
		beego.Debug(res)
		this.ServeJSON()
		return
	}
	code = models.DelLists(delTodoLists, user.UserID)
	if code != 1000 {
		res.Msg = ErrInsertDB
		res.Code = ErrInsertDBCode
		this.Data["json"] = res
		beego.Debug(res)
		this.ServeJSON()
		return
	}
	res.Code = code
	this.Data["json"] = res
	beego.Debug(res)
	this.ServeJSON()
}

//SaveTodo method.
func (this *TodoLists) SaveTodo() {

	user, code := this.CheckToken()
	res := this.InitResponseModel()

	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		this.Data["json"] = res
		this.ServeJSON()
		return
	}

	content := this.GetString("content")
	if len(content) == 0 || content == "" {
		res.Code = ErrParmarsCode
		res.Msg = ErrParmars
		this.Data["json"] = res
		this.ServeJSON()
		return
	}

	stamp := int(time.Now().Unix())
	t := models.TodoList{}
	t.CreatedAt = stamp
	t.UpdatedAt = stamp
	t.Content = content
	t.UserID = user.UserID
	resMsg, code := t.SaveTodo()
	res.Msg = resMsg
	res.Code = code
	res.Data["todo"] = t
	this.Data["json"] = res
	this.ServeJSON()
}

//TodoLists method.
func (ts *TodoLists) TodoLists() {
	beego.Debug(ts.Input())
	user, code := ts.CheckToken()
	res := ts.InitResponseModel()

	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		ts.Data["json"] = res
		ts.ServeJSON()
		return
	}

	start, err1 := ts.GetInt("start")
	if err1 != nil {
		start = 0
	}
	end, err2 := ts.GetInt("end")
	if err2 != nil {
		end = int(time.Now().Unix())
	}

	beego.Debug("USERID: ", user.UserID, "START: ", start, "END:", end)
	todoLists := models.FindTodoLists(user.UserID, start, end)
	res.Code = 1000
	res.Data["list"] = todoLists
	ts.Data["json"] = res
	ts.ServeJSON()
}

//UpdateTodo method.
func (this *TodoLists) UpdateTodo() {
	user, code := this.CheckToken()
	res := this.InitResponseModel()

	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		this.Data["json"] = res
		this.ServeJSON()
		return
	}

	content := this.GetString("content")
	todoID, err1 := this.GetInt("todoId")
	status, err2 := this.GetInt("isOver")
	if err1 != nil || ((err2 != nil) && len(content) == 0) {
		res.Code = ErrParmarsCode
		res.Msg = ErrParmars
		this.Data["json"] = res
		this.ServeJSON()
		return
	}

	t := models.TodoList{}
	t.UserID = user.UserID
	t.TodoID = todoID
	t.Content = content
	t.Status = status
	resMsg, code := t.UpdateTodo()
	res.Msg = resMsg
	res.Code = code
	this.Data["json"] = res
	this.ServeJSON()
}
