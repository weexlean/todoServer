package controllers

import (
	"AlanGo/models"

	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	// c.Data["Website"] = "beego.me"
	c.Data["forgetHtml"] = "localhost:8080/v1/users/ForgetTpl"
	c.Data["registerHtml"] = "localhost:8080/register.tpl"
	c.TplName = "login.tpl"
}

func (c *MainController) Prepare() {
	c.Ctx.ResponseWriter.Header().Set("Access-Control-Allow-Origin", "*")                           //允许访问源
	c.Ctx.ResponseWriter.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS")    //允许post访问
	c.Ctx.ResponseWriter.Header().Set("Access-Control-Allow-Headers", "Content-Type,Authorization") //header的类型
	c.Ctx.ResponseWriter.Header().Set("Access-Control-Max-Age", "1728000")
	c.Ctx.ResponseWriter.Header().Set("Access-Control-Allow-Credentials", "true")
	// c.Ctx.ResponseWriter.Header().Set("content-type", "application/json") //返回数据格式是json
}

//InitResponseModel method
func (c *MainController) InitResponseModel() (res ResponseData) {
	_res := ResponseData{}
	res = _res
	res.Data = make(map[string]interface{})
	res.Parmars = c.Input()
	return
}

//CheckToken method.
func (c *MainController) CheckToken() (user models.User, code int) {
	UserID := c.GetString("userId")
	token := c.GetString("token")
	user, code = models.FindUserByUserId(UserID)
	beego.Debug("USER: ", UserID, "TOKEN: ", token)
	if code != 1000 {
		beego.Debug(user, code)
		code = 1004
		return
	}
	if user.Token != token {
		beego.Debug(user.Token, token)
		code = ErrTokenSessionCode
		return
	}
	code = 1000
	return
}
