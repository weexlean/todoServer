package controllers

import (
	"AlanGo/models"
	"container/list"

	"github.com/gorilla/websocket"
)

//Subscriber models
type Subscriber struct {
	UserID string
	Conn   *websocket.Conn
}

var (
	subscribe   = make(chan Subscriber, 10)
	unsubscribe = make(chan string, 10)
	publish     = make(chan models.Event, 10)
	waitingList = list.New()
	// subscribers = list.New()
	subscribers = make(map[string]*websocket.Conn)
)

func JoinRoom(userId string, ws *websocket.Conn) {
	if subscribers[userId] == nil {
		subscribers[userId] = ws
		subscribe <- Subscriber{UserID: userId, Conn: ws}
	}
}

func addEvent(event models.Event) {
	waitingList.PushBack(event)
}

func init() {
	go chatroom()
}

func newEvent(ep models.EventType, user string, sessionId string, time string, msg string) models.Event {
	return models.Event{ep, user, sessionId, time, msg}
}

func Join(userId string, ws *websocket.Conn) {
	subscribe <- Subscriber{UserID: userId, Conn: ws}
}

func leave(user string) {
	unsubscribe <- user
}

func chatroom() {
	// for {
	// 	select {
	// 	case sub := <-subscribe:
	//
	// 		if !isUserExist(subscribers, sub.UserID) {
	//
	//     } else {
	//
	//     }
	// 			publish <- newEvent(models.EVENT_JOIN, sub.UserID, "")
	// 			beego.Debug("New user: ", sub.UserID, "WebSocket: ", sub.Conn != nil)
	// 		} else {
	// 			beego.Debug("Old user: ", sub.UserID, "Websocket: ", sub.Conn != nil)
	// 		}
	// 	case event := <-publish:
	// 		for ch := waitingList.Back(); ch != nil; ch = ch.Prev() {
	// 			ch.Value.(chan bool) <- true
	// 			waitingList.Remove(ch)
	// 		}
	// 		// broadcastWebSocket(event)
	// 	}
	// }
}

func isUserExist(subscribes *list.List, userId string) bool {
	for sub := subscribes.Front(); sub != nil; sub = sub.Next() {
		if sub.Value.(Subscriber).UserID == userId {
			return true
		}
	}
	return false
}
