package utils

import (
	"database/sql"

	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
)

var SQLDB *sql.DB

func connectDB() *sql.DB {
	return SQLDB
}

func InitSql() {
	dbPath := beego.AppConfig.String("mysql::url")
	db, error := sql.Open("mysql", dbPath)
	if error != nil {
		panic(error)
	}
	error = db.Ping()
	if error != nil {
		panic(error)
	}
	SQLDB = db
}
