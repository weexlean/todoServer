package controllers

//Err model define
const (
	ErrNoUser            = "该账号不存在"
	ErrPassErr           = "密码不正确"
	ErrInsertDB          = "插入数据库失败"
	ErrParmars           = "参数不全"
	ErrUserNameOrPass    = "账号或密码有误"
	ErrTokenSession      = "Token 已过期"
	ErrWebsocketConnFail = "长连接建立失败"
	ErrUploadFileFail    = "上传文件失败"
)

//Err models
const (
	ErrDatabase = -1
	ErrSystem   = -2
	ErrDupRows  = -3
	ErrNotFound = -4
)

//CodeInfo model define.
type CodeInfo struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Data map[string]interface{} `json:"data"`
}

// func InitCodeInfo() (info CodeInfo) {
// 	info = CodeInfo
// }

//Err model define
const (
	ErrInsertDBCode          = 1001
	ErrParmarsCode           = 9999
	ErrUpdateDB              = 1002
	ErrUserExists            = 1003
	ErrUserNameOrPassCode    = 1004
	ErrTokenSessionCode      = 1005
	ErrWebsocketConnFailCode = 1006
	ErrUploadFileFailCode    = 1007
)

//ResponseData model define
type ResponseData struct {
	Code    int                    `json:"code"`
	Msg     string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
	Parmars interface{}            `json:"parmars"`
}
