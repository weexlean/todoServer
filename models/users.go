package models

import (
	"AlanGo/models/sqlDB"

	"github.com/astaxie/beego"
)

//User model define
type User struct {
	ID        int64  `json:"id"`
	UserID    string `json:"userId"`
	Username  string `json:"username"`
	Password  string `json:"password,"`
	Avatar    string `json:"avator"`
	Token     string `json:"token"`
	Age       int    `json:"age"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Status    int    `json:"status"`
	LastLogin int64  `json:"last_login"`
	CreatedAt int64  `json:"create_at"`
	UpdatedAt int64  `json:"update_at"`
}

//Login method.
func (us *User) Login() (code int) {
	db := sqlDB.Conn()
	user, c := FindUser(us.Username)
	if c != 1000 {
		code = 99999
		beego.Debug("user", user, "code", c)
		return
	}
	if user.Password != us.Password {
		code = 1004
		beego.Debug("user", user, "code", code)
		return
	}

	username := us.Username
	password := us.Password

	SQL2 := "UPDATE USER SET TOKEN=? WHERE UserId=?"
	_, err := db.Exec(SQL2, us.Token, user.UserID)
	if err != nil {
		code = 99999
		beego.Debug("Login...: ", err)
		return
	}

	SQL := "SELECT * FROM USER WHERE USERNAME=? AND PASS=?"
	rows2 := db.QueryRow(SQL, username, password)
	err = rows2.Scan(&us.ID, &us.UserID, &us.Username, &us.Password, &us.Token, &us.LastLogin, &us.CreatedAt, &us.UpdatedAt, &us.Avatar)
	if err != nil {
		code = 99999
		beego.Debug("err: ", err)
		return
	}
	code = 1000
	beego.Debug("Login: ", err)
	return
}

//Register method.
func (us *User) Register() (err error) {
	db := sqlDB.Conn()
	SQL := "INSERT INTO USER (UserID ,USERNAME, PASS, TOKEN, CREATEDAT, UPDATEDAT) VALUES(?,?,?,?,?,?)"
	results, er := db.Exec(SQL, us.UserID, us.Username, us.Password, us.Token, us.CreatedAt, us.UpdatedAt)
	if er != nil {
		err = er
		beego.Debug("Register: ", er)
		return
	}
	_, er2 := results.RowsAffected()
	if er2 != nil {
		err = er2
		beego.Debug("Register: ", er2)
		return
	}
	return
}

func FindUserByName(name string) (users []User) {
	db := sqlDB.Conn()
	users = make([]User, 0)
	results, err := db.Query("SELECT * FROM USER WHERE USERNAME LIKE ?", "%"+name+"%")
	defer results.Close()
	if err != nil {
		beego.Debug(err)
		return
	}

	for results.Next() {
		var us User
		err := results.Scan(&us.ID, &us.UserID, &us.Username, &us.Password, &us.Token, &us.LastLogin, &us.CreatedAt, &us.UpdatedAt, &us.Avatar)
		if err != nil {
			beego.Debug(err)
		}
		users = append(users, us)
	}
	return users
}

//FindUserByUserId method.
func FindUserByUserId(UserId string) (user User, code int) {
	db := sqlDB.Conn()
	// defer db.Close()
	code = 1000
	rows, err := db.Query("SELECT * FROM USER WHERE UserId=?", UserId)
	defer rows.Close()
	if err != nil {
		beego.Debug(err)
		code = 99999
		return
	}
	user = User{}
	if rows.Next() {
		err = rows.Scan(&user.ID, &user.UserID, &user.Username, &user.Password, &user.Token, &user.LastLogin, &user.CreatedAt, &user.UpdatedAt, &user.Avatar)
		if err != nil {
			beego.Debug(err)
			code = 99999
			return
		}
	}
	code = 1000
	return
}

func (u *User) AddFriend(userId string) (user User, code int) {
	if len(userId) == 0 {
		code = 9999
		beego.Debug("AddFriend: ", u.UserID, userId, 9999)
		return
	}

	user, c := IsUserExists(userId)
	if c != 1000 {
		code = 1003
		return
	}

	db := sqlDB.Conn()
	_, err := db.Exec("INSERT INTO FRIENDS (USERID, FRIENDID) VALUE(?,?)", u.UserID, userId)
	if err != nil {
		code = 1001
		beego.Debug(err)
		return
	}
	code = 1000
	return
}

func IsUserExists(userId string) (user User, code int) {
	db := sqlDB.Conn()
	row := db.QueryRow("SELECT * FROM USER WHERE USERID=?", userId)
	user = User{}
	error := row.Scan(&user.ID, &user.UserID, &user.Username, &user.Password, &user.Token, &user.LastLogin, &user.CreatedAt, &user.UpdatedAt, &user.Avatar)
	if error != nil {
		beego.Debug(error)
		code = 9999
		return
	}
	if len(user.UserID) == 0 {
		code = 1003
		return
	}
	code = 1000
	return
}

func (u *User) UpdateUser() (code int) {
	db := sqlDB.Conn()
	_, err := db.Exec("UPDATE USER SET Avatar=? WHERE USERID=?", u.Avatar, u.UserID)
	if err != nil {
		beego.Debug(err)
		code = 1001
	} else {
		code = 1000
	}
	return
}

//FindUser method.
func FindUser(name string) (user User, code int) {
	db := sqlDB.Conn()
	results := db.QueryRow("SELECT * FROM USER WHERE UserId=MD5(?)", name)
	user = User{}
	error := results.Scan(&user.ID, &user.UserID, &user.Username, &user.Password, &user.Token, &user.LastLogin, &user.CreatedAt, &user.UpdatedAt, &user.Avatar)
	if error != nil {
		code = 99999
	} else {
		code = 1000
	}
	beego.Debug("name: ", name, "user: ", user, "error: ", error)
	return
}
