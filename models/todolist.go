package models

import (
	"AlanGo/models/sqlDB"

	"github.com/astaxie/beego"
)

//TodoList model define
type TodoList struct {
	UserID    string `json:"userId"`
	TodoID    int    `json:"todoId"`
	Content   string `json:"content"`
	Status    int    `json:"isOver"`
	CreatedAt int    `json:"stamp"`
	UpdatedAt int
}

// SaveTodo add todo
func (t *TodoList) SaveTodo() (resMsg string, code int) {
	db := sqlDB.Conn()
	sql1 := "INSERT INTO TODOLIST (USERID, CONTENT, CREATEDAT, UPDATEDAT) VALUES(?,?,?,?)"
	beego.Debug(t)
	result, err := db.Exec(sql1, t.UserID, t.Content, t.CreatedAt, t.UpdatedAt)
	if err != nil {
		beego.Debug(err)
		resMsg = "数据错误"
		code = 1001
		return
	}
	id, _ := result.LastInsertId()
	t.TodoID = int(id)
	resMsg = "插入数据成功"
	code = 1000
	return
}

//DelLists method.
func DelLists(todos []string, userId string) (code int) {
	db := sqlDB.Conn()
	beego.Debug(userId)

	for _, b := range todos {
		_, err := db.Exec("UPDATE TODOLIST SET STATUS=2 WHERE USERID=? AND TODOID=?", userId, b)
		if err != nil {
			code = 1001
			beego.Debug(err)
			return
		}
	}

	code = 1000
	return
}

func FindTodoLists(userId string, start int, end int) (lists []TodoList) {
	db := sqlDB.Conn()

	SQL := "SELECT * FROM TODOLIST WHERE USERID=? AND CREATEDAT>? AND CREATEDAT<? AND (STATUS=0 OR STATUS=1)"
	results, err := db.Query(SQL, userId, start, end)
	if err != nil {
		beego.Debug(err)
	}
	defer results.Close()
	lists = make([]TodoList, 0)
	for results.Next() {
		todo := TodoList{}
		err = results.Scan(&todo.TodoID, &todo.UserID, &todo.Content, &todo.Status, &todo.CreatedAt, &todo.UpdatedAt)
		beego.Debug("FindTodoLists: ", err)
		lists = append(lists, todo)
	}
	beego.Debug("FindTodoLists: ", lists)
	return
}

//UpdateTodo method.
func (this *TodoList) UpdateTodo() (resMsg string, code int) {
	db := sqlDB.Conn()
	if len(this.Content) != 0 {
		SQL := "UPDATE TODOLIST SET CONTENT=? WHERE TODOID=? AND USERID=?"
		_, err := db.Exec(SQL, this.Content, this.TodoID, this.UserID)
		if err != nil {
			resMsg = "数据库操作失败"
			code = 1002
			beego.Debug(err)
			return
		}
		resMsg = "更新数据成功"
		code = 1000
		return
	}
	_, err := db.Exec("UPDATE TODOLIST SET STATUS=? WHERE TODOID=? AND UserID=?", this.Status, this.TodoID, this.UserID)
	if err != nil {
		resMsg = "数据库操作失败"
		beego.Debug(err)
		code = 1002
		return
	}
	resMsg = "更新数据成功"
	code = 1000
	return

}
