package controllers

import (
	"AlanGo/models"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
)

type UserController struct {
	MainController
}

// func (c *UserController) RegisterTpl() {
// 	c.TplName = "register.tpl"
// }

func (c *UserController) QueryFriend() {
	_, code := c.CheckToken()
	res := c.InitResponseModel()
	beego.Debug(res)
	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}
	friendName := c.GetString("username")
	if len(friendName) == 0 {
		res.Code = ErrParmarsCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}

	users := models.FindUserByName(friendName)
	res.Data["users"] = users
	res.Code = code
	c.Data["json"] = res
	beego.Debug(res)
	c.ServeJSON()
	return
}

func (c *UserController) AddFriend() {
	user, code := c.CheckToken()
	res := c.InitResponseModel()
	beego.Debug(res)
	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}

	friendID := c.GetString("friendId")
	res.Data["user"], res.Code = user.AddFriend(friendID)
	c.Data["json"] = res
	beego.Debug(res)
	c.ServeJSON()
}

//SendMessage methods
func (c *UserController) SendMessage() {
	user, code := c.CheckToken()
	res := c.InitResponseModel()
	beego.Debug(res)
	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}

	eventType := c.GetString("eventType")
	content := c.GetString("content")
	sessionID := c.GetString("sessionID")
	time := c.GetString("timestamp")

	if len(eventType) == 0 || len(content) == 0 || len(sessionID) == 0 || len(time) == 0 {
		res.Msg = ErrParmars
		res.Code = ErrParmarsCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}

	event := newEvent(models.EVENT_MESSAGE, user.UserID, sessionID, time, content)
	addEvent(event)
	res.Code = 1000
	c.Data["json"] = res
	c.ServeJSON()
	return
}

//Upload method
func (c *UserController) Upload() {
	user, code := c.CheckToken()
	res := c.InitResponseModel()
	beego.Debug(res)
	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}

	file, head, err := c.GetFile("file")
	defer file.Close()
	if err != nil {
		beego.Debug(err)
		return
	}
	body, err := ioutil.ReadAll(file)
	md := md5.New()
	md5 := fmt.Sprintf("%x", md.Sum(body))

	err = c.SaveToFile(head.Filename, "./static/md5")
	if err != nil {
		beego.Debug(err)
		res.Msg = ErrUploadFileFail
		res.Code = ErrUploadFileFailCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}
	user.Avatar = "/static/" + md5
	res.Code = user.UpdateUser()
	res.Data["file"] = md5
	c.Data["json"] = res
	c.ServeJSON()
}

func (c *UserController) Join() {
	user, code := c.CheckToken()
	res := c.InitResponseModel()
	beego.Debug(res)
	if code != 1000 {
		res.Msg = ErrTokenSession
		res.Code = ErrTokenSessionCode
		c.Data["json"] = res
		beego.Debug(res)
		c.ServeJSON()
		return
	}
	ws, err := websocket.Upgrade(c.Ctx.ResponseWriter, c.Ctx.Request, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		res.Msg = "Not a websocket handshake"
		res.Code = 1006
		c.Data["json"] = res
		c.ServeJSON()
		return
	} else if err != nil {
		beego.Error("Cannot setup WebSocket connection:", err)
		res.Msg = "Cannot setup WebSocket connection:"
		res.Code = 1006
		c.Data["json"] = res
		c.ServeJSON()
		return
	}
	JoinRoom(user.UserID, ws)
	res.Code = 1000
	ws.WriteJSON(res)
}

func (c *UserController) Login() {
	beego.Debug("Login...")
	res := c.InitResponseModel()
	user := models.User{}
	user.Username = c.GetString("username")
	user.Password = c.GetString("password")

	md := md5.New()
	stamp := strconv.FormatInt(time.Now().Unix(), 10)
	io.WriteString(md, user.Username+stamp)
	token := fmt.Sprintf("%x", md.Sum(nil))
	beego.Debug("token: ", user.Username, stamp, token)
	if user.Username == "" || len(user.Username) == 0 || user.Password == "" || len(user.Password) == 0 {
		res.Code = ErrParmarsCode
		res.Msg = ErrParmars
		c.Data["json"] = res
		c.ServeJSON()
		return
	}

	beego.Debug("mdt: ", token)
	user.Token = token

	code := user.Login()
	if code != 1000 {
		res.Code = ErrUserNameOrPassCode
		res.Msg = ErrUserNameOrPass
		c.Data["json"] = res
		beego.Debug("err: ", res)
		c.ServeJSON()
		return
	}

	res.Code = 1000
	res.Data["user"] = user
	c.Data["json"] = res
	c.ServeJSON()
}

func (c *UserController) Register() {
	beego.Debug("Register:")
	res := c.InitResponseModel()
	username := c.GetString("username")
	password := c.GetString("password")
	cPassword := c.GetString("cpassword")
	beego.Info("Register:", c.Input())
	if username == "" || len(username) == 0 || password == "" || len(password) == 0 || password != cPassword {
		res.Msg = "参数有误"
		res.Code = ErrParmarsCode
		c.Data["json"] = res
		c.ServeJSON()
		return
	}

	_, code := models.FindUser(username)
	if code == 1000 {
		res.Msg = "该用户已存在"
		res.Code = ErrUserExists
		c.Data["json"] = res
		c.ServeJSON()
		return
	}

	md := md5.New()
	io.WriteString(md, username)
	userID := fmt.Sprintf("%x", md.Sum(nil))
	user := models.User{}
	user.UserID = userID
	user.Username = username
	user.Password = password
	user.CreatedAt = time.Now().Unix()
	user.UpdatedAt = user.CreatedAt
	user.LastLogin = 0

	error := user.Register()
	if error != nil {
		res.Msg = "数据库错误"
		res.Code = ErrInsertDBCode
		c.Data["json"] = res
		c.ServeJSON()
		return
	}

	use, _ := models.FindUser(username)
	res.Code = 1000
	res.Data["user"] = use
	c.Data["json"] = res
	c.ServeJSON()
	return
}

func (c *UserController) ForgetTpl() {
	c.TplName = "forget.tpl"
}

func (c *UserController) Getlist() {
	c.TplName = "list.tpl"
}

// func (c *UserController) Post() {
// 	var v models.User
// 	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
// 		// if errMessage := utils.che
// 		token := c.Ctx.Input.Header("token")
// 		idStr := c.Ctx.Input.Param(":id")
// 		token
// 		idStr
// 	}
// }

///test
func (c *UserController) a1() {
	var v models.User
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		c.Ctx.ResponseWriter.WriteHeader(403)
	}

}
