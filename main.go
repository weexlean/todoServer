package main

import (
	_ "AlanGo/routers"
	"AlanGo/utils"
	"runtime"

	"github.com/astaxie/beego"
)

func main() {
	num := runtime.NumCPU() //本地机器的逻辑CPU个数
	runtime.GOMAXPROCS(num) //设置可同时执行的最大CPU数，并返回先前的设置
	utils.InitBootStrap()
	beego.Run()
	// cache.NewCache(adapterName, config)
}
